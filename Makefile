SHELL=/bin/sh

.PHONY: all

all: latex

task-1: pdflatex-task-1 lint-task-1

pdflatex-task-1:
	pdflatex task_1.tex

lint-task-1:
	git add -u task_1.tex task_1_title.tex
	which z-djail && \
		test -d latexindent.pl-master && \
		z-djail \
			perl latexindent.pl-master/latexindent.pl \
				--overwrite \
			  	--modifylinebreaks \
					task_1.tex task_1_title.tex

show-task-1: task-1
	xdg-open task_1.pdf

test:
	PYTHONPATH=. ./venv/bin/pytest -v tests/test*.py

jupyter-lab:
	./venv/bin/jupyter lab

measure-task-1:
	z-cpufreq-800
	./venv/bin/python task_1.py "CSV"
	z-cpufreq-4300

measure-task-1-quick:
	./venv/bin/python task_1.py "CSV"

measure-task-1-test:
	env TEST_MODE=true ./venv/bin/python task_1.py "CSV"

plot-task-1:
	./venv/bin/python task_1.py "PLOT"
