from typing import List


def direct_calculation(x: int, coefficients: List[int]):
    """
    The `coefficients` array is expected to have the coefficients to
    represents the polynomial:

        x^0 * coefficients[0] + x^1 * coefficients[1] + x^2 * coefficients[2] + ...

    """
    # sum (k=1 -> n) v_k * x^(k-1)
    n = len(coefficients)
    result = 0
    for k in range(0, n):
        term = coefficients[k] * x ** k
        result += term

    return result


def horners_method(x: float, coefficients: List[int], current_pos=0):
    """
    The `coefficients` array is expected to have the coefficients to
    represents the polynomial:

        x^0 * coefficients[0] + x^1 * coefficients[1] + x^2 * coefficients[2] + ...

    """
    assert len(coefficients) > 0

    if current_pos == 0:
        if len(coefficients) == 1:
            return coefficients[0]
        else:
            return coefficients[0] + horners_method(x, coefficients, 1)

    if current_pos == len(coefficients) - 1:
        return coefficients[current_pos] * x

    return x * (coefficients[current_pos] +
                horners_method(x, coefficients, current_pos + 1))
