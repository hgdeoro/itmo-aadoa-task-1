import random


# --------------------------------------------------------------------------------
# QuickSort implementation from https://realpython.com/sorting-algorithms-python/
# Retrieval date: 2020-09-16
# --------------------------------------------------------------------------------


def bubble_sort(array):
    n = len(array)

    for i in range(n):
        # Create a flag that will allow the function to
        # terminate early if there's nothing left to sort
        already_sorted = True

        # Start looking at each item of the list one by one,
        # comparing it with its adjacent value. With each
        # iteration, the portion of the array that you look at
        # shrinks because the remaining items have already been
        # sorted.
        for j in range(n - i - 1):
            if array[j] > array[j + 1]:
                # If the item you're looking at is greater than its
                # adjacent value, then swap them
                array[j], array[j + 1] = array[j + 1], array[j]

                # Since you had to swap two elements,
                # set the `already_sorted` flag to `False` so the
                # algorithm doesn't finish prematurely
                already_sorted = False

        # If there were no swaps during the last iteration,
        # the array is already sorted, and you can terminate
        if already_sorted:
            break

    return array


# --------------------------------------------------------------------------------
# QuickSort implementation from https://stackoverflow.com/a/17773584
# Retrieval date: 2020-09-20
# --------------------------------------------------------------------------------

def _sub_partition(array, start, end, idx_pivot):
    """
    returns the position where the pivot winds up
    """

    if not (start <= idx_pivot <= end):
        raise ValueError('idx pivot must be between start and end')

    array[start], array[idx_pivot] = array[idx_pivot], array[start]
    pivot = array[start]
    i = start + 1
    j = start + 1

    while j <= end:
        if array[j] <= pivot:
            array[j], array[i] = array[i], array[j]
            i += 1
        j += 1

    array[start], array[i - 1] = array[i - 1], array[start]
    return i - 1


def quicksort(array, start=0, end=None):
    if end is None:
        end = len(array) - 1

    if end - start < 1:
        return

    idx_pivot = random.randint(start, end)
    i = _sub_partition(array, start, end, idx_pivot)
    quicksort(array, start, i - 1)
    quicksort(array, i + 1, end)
