import numpy as np


def _calculate_cell(matrix_a, matrix_b, target_x, target_y):
    result = 0
    for i in range(0, len(matrix_a)):
        result += matrix_a[target_x][i] * matrix_b[i][target_y]
    return result


def matrix_product(matrix_a, matrix_b):
    """Only for square matrices"""
    size = len(matrix_a)
    assert len(matrix_b) == size
    assert len(matrix_a[0]) == size
    assert len(matrix_b[0]) == size

    result_naive = np.zeros((size, size), dtype=int)
    for x in range(0, size):
        for y in range(0, size):
            result_naive[x][y] = _calculate_cell(matrix_a, matrix_b, x, y)
    return result_naive
