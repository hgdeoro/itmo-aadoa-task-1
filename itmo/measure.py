import abc
import gc
import logging
import pathlib
import time
import typing

import numpy as np


class Measure(abc.ABC):
    MEASUREMENT_MS_FORMAT = "{time_ms}"

    def __init__(self, repeat: int, output_filename: pathlib.Path,
                 output_f_args: typing.List[str]):
        self._repeat: int = repeat
        self._output_filename: pathlib.Path = output_filename
        self._output_f_args: typing.List[str] = output_f_args

    @abc.abstractmethod
    def _get_f_args(self) -> typing.Iterable:
        raise NotImplementedError()

    @abc.abstractmethod
    def build_function_data(self, size):
        pass

    @abc.abstractmethod
    def run_function(self, arrays):
        pass

    def _create_random_list(self,
                            size: typing.List[int],
                            astype=np.int,
                            max_random=2 ** 8):
        array = (np.floor(np.random.rand(*size) * max_random) + 1).astype(astype)
        return array.tolist()

    def run(self):
        print(f"run() -> {self._output_filename.name}")
        with self._output_filename.open('w') as output:
            output.write(f"{','.join(self._output_f_args)},avg_ms\n")
            for f_args in self._get_f_args():
                output_f_args_values = [str(f_args[k]) for k in self._output_f_args]
                print(f"f_args={output_f_args_values}")

                try:  # disable gc - enable it in finally block
                    gc.disable()
                    elapsed_sec = 0.0
                    for _ in range(self._repeat):
                        function_data = self.build_function_data(**f_args)
                        timestamp_start = time.monotonic()
                        self.run_function(function_data)
                        timestamp_end = time.monotonic()
                        elapsed_sec += (timestamp_end - timestamp_start)
                except OverflowError:
                    logging.exception("Call of function failed with OverflowError")
                    elapsed_sec = None
                finally:
                    gc.enable()

                if elapsed_sec is not None:
                    elapsed_ms = elapsed_sec * 1000
                    avg_ms = elapsed_ms / self._repeat
                    avg_ms_formatted = self.MEASUREMENT_MS_FORMAT.format(time_ms=avg_ms)
                    output.write(
                        f"{','.join(output_f_args_values)},{avg_ms_formatted}\n")
