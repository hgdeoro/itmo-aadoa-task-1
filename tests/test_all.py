import numpy as np

from itmo import polynomial, matrix


def test_direct_calculation():
    assert polynomial.direct_calculation(0, [0]) == 0
    assert polynomial.direct_calculation(0, [0, 0]) == 0
    assert polynomial.direct_calculation(0, [0, 0, 0]) == 0
    assert polynomial.direct_calculation(0, [2, 4, 6]) == 2
    assert polynomial.direct_calculation(1, [2, 4, 6]) == 2 + 4 + 6
    assert polynomial.direct_calculation(2, [2, 4, 6]) == 2 + 4 * 2 + 6 * 4

    assert polynomial.horners_method(0, [0]) == 0
    assert polynomial.horners_method(0, [0, 0]) == 0
    assert polynomial.horners_method(0, [0, 0, 0]) == 0
    assert polynomial.horners_method(0, [2, 4, 6]) == 2
    assert polynomial.horners_method(1, [2, 4, 6]) == 2 + 4 + 6
    assert polynomial.horners_method(2, [2, 4, 6]) == 2 + 4 * 2 + 6 * 4

    # 2*x^4 - x^3 + 3*x^2  + x - 5 = 160
    book_example = [2, -1, 3, 1, -5]
    assert polynomial.horners_method(3, list(reversed(book_example))) == 160


def test_matrix_product():
    size = 5
    matrix_a = np.ceil(np.random.rand(size, size) * 2 ** 5).astype(np.int)
    matrix_b = np.ceil(np.random.rand(size, size) * 2 ** 5).astype(np.int)
    result_np = np.matmul(matrix_a, matrix_b)

    for array in matrix_a:
        for value in array:
            assert value > 0

    for array in matrix_b:
        for value in array:
            assert value > 0

    result_naive = matrix.matrix_product(matrix_a, matrix_b)

    for x in range(0, size):
        for y in range(0, size):
            assert result_naive[x][y] == result_np[x][y]
