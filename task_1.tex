\documentclass[a4paper,14pt,final]{article}

%--------------------------------------------------
% imports
%--------------------------------------------------

% basic imports
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
% pic in cover
\usepackage[cc]{titlepic}
% listings
\usepackage{listings}
\usepackage{float}
% indent
\usepackage{indentfirst}
% fixme
\usepackage[layout={inline,index}]{fixme}

%--------------------------------------------------
% setup listing format
%--------------------------------------------------


\definecolor{listings_commentstyle}{rgb}{0,0.6,0}
\definecolor{listings_numberstyle}{rgb}{0.5,0.5,0.5}
\definecolor{listings_codepurple}{rgb}{0.58,0,0.82}
\definecolor{listings_backcolour}{rgb}{0.95,0.95,0.95}
%\definecolor{listings_keywordstyle}{magenta}

\lstdefinestyle{python_style}{
    backgroundcolor=\color{listings_backcolour},
    commentstyle=\color{listings_commentstyle},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{listings_numberstyle},
    stringstyle=\color{listings_codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=4
}

\lstset{style=python_style}

%--------------------------------------------------
% FIXMEs
%--------------------------------------------------

%\fxsetup{status=draft,envlayout=color}
\fxsetup{envlayout=color}

%--------------------------------------------------
% title, toc
%--------------------------------------------------

\title{Task 1\\Experimental time complexity analysis}
\author{Horacio Guillermo De Oro}
\date{2020}

% \titlepic{}



\begin{document}

\clearpage

%\maketitle

\input{task_1_title.tex}

\thispagestyle{empty}

\newpage

\pagenumbering{roman}
\tableofcontents
\newpage
\pagenumbering{arabic}

%--------------------------------------------------
% Goal
%--------------------------------------------------

\section{Goal}

Experimental study of the time complexity of different algorithms.

%--------------------------------------------------
% Formulation of the problem
%--------------------------------------------------

\section{Formulation of the problem}

For each n from 1 to 2000, measure the average computer execution time(using timestamps) of programs implementing the selected algorithms, plot the data obtained showing the average execution time as a function of n. Conduct the theoretical analysis of the time complexity of the algorithms in question and compare the empirical and theoretical time complexities.

\newpage

%--------------------------------------------------
% Brief theoretical part
%--------------------------------------------------

\section{Theoretical information related to the task}

\subsection{Algorithms}

\subsubsection{Constant function}

For the empirical evaluation, a simple algorithm was implemented.\\

The function returns the first element of the Python list.
Time complexity is $\Theta(1)$.

\subsubsection{Sum of elements}

For the empirical evaluation, a simple algorithm was implemented.\\

The function returns the sum of all the elements of the Python list.
Time complexity is $\Theta(n)$.

\subsubsection{Product of elements}

For the empirical evaluation, a simple algorithm was implemented.\\

The function returns the product of all the elements of the Python list.
The time complexity of the naive implementation is $\Theta(n^2)$.

\subsubsection{Calculation of polynomial: direct method}

For the empirical evaluation, the algorithm was implemented.\\

The function returns the value of $P(1.5)$ by evaluating each of the terms of the polynomial and accumulating the partial result.
Time complexity is $\Theta(n)$.

\subsubsection{Calculation of polynomial: Horner's method}

For the empirical evaluation, the algorithm was implemented.\\

The algorithm returns the value of $P(1.5)$ by recursively evaluating each of the terms of the polynomial using Horner's method. Implementation is naive, does $\Theta(n)$ multiplications and $\Theta(n)$ additions.\\

Time complexity is $\Theta(n)$.

\subsubsection{Bubble sort}

For the empirical evaluation, an existing implementation of the algorithm was used.\\

The algorithm uses a \textit{brute-force} approach.
Time complexity is supposed to be $\Theta(n^2)$. % in the worst and average cases

\subsubsection{Quick sort}

For the empirical evaluation, an existing implementation of the algorithm was used.\\

The algorithm uses a \textit{divide-and-conquer} approach, dividing the elements of the array according to their value.
Average time complexity is in the order of $\Theta(n * log(n))$.

\subsubsection{Tim sort}

For the empirical evaluation, the built-in Python function was used for in-place sort of lists.\\

Algorithm uses divide-and-conquer and decrease-and-conquer techniques (internally it uses both Merge Sort and Insertion Sort).
Average time complexity is in the order of $\Theta(n * log(n))$, and best
time complexity is in the order of $\Theta(n)$.

\subsubsection{Matrix product}

For the empirical evaluation, the algorithm was implemented.\\

The time complexity of square matrices is $\Theta(n^3)$.

%--------------------------------------------------
% Results
%--------------------------------------------------

\newpage

\section{Results}

\subsection{Empirical evaluation details}

\subsubsection{Run time environment}

The task 1 was done using Python 3.7.7 on GNU/Linux. Some algorithms were developed and other were obtained from the internet. All algorithms can be classified as \textit{naive implementations}.\\

In this document, when a reference to \textit{array} is found, it could mean \textit{Python list}.\\

\subsubsection{Generated data}

Arrays and matrices were created using NumPy but converted into standard Python list, to make sure algorithms doesn't take advantage of the optimizations provided by NumPy data structure.\\

Array are in fact \textit{Python list}, and matrices are \textit{list of lists}.

\subsubsection{Method to measure execution time}

There are some sources of variability in the execution time of the algorithms that we can easily managed:

\textbf{Garbage collector}: to avoid any negative effect of Python's garbage collector, it is disabled before running the algorithm under study.

\textbf{CPU temperature and turbo mode}: to avoid fluctuations of CPU performance produced by variations of CPU temperature and "Turbo" modes, all tests were executed with CPU configured at the lowest speed. For \textit{Intel Core i7-8850H CPU} the processor base frequency is \textit{2.60GHz}, the lowest frequency is \textit{800Mhz} and the "Turbo Boost" frequency is \textit{4.30GHz}.

\textbf{Creation of data}: The data required to execute the algorithms is produced in a different context than the execution of the algorithm.

\textbf{Timer precision}: The timestamps used to measure execution time are \textit{monotonic}, we use Python's standard library \textit{time.monotonic()}.

\textbf{CPU cache}: Efficiency of different levels of CPU cache can introduce variability in the obtained results. For simplicity reasons, this was ignored in the analysis.

The code used to perform the timing is:

\lstinputlisting[language=Python, caption=Code to measure time]{snippets/timig.py}

\subsection{Constant function}

Empirical showed time complexity of $\Theta(1)$, which matches theoretical time complexity.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{data/constant_function.pdf}
	\caption{Performance of constant function}
	\label{fig:constant_function}
\end{figure}

\subsection{Sum of elements}

Empirical showed time complexity of $\Theta(n)$, which matches theoretical time complexity.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{data/sum_of_elements.pdf}
	\caption{Performance of sum of element of vector}
	\label{fig:sum_of_elements}
\end{figure}

\subsection{Product of elements}

Empirical showed time complexity of $\Theta(n^2)$, which matches theoretical time complexity.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{data/product_of_elements.pdf}
	\caption{Performance product of non-zero element of vector}
	\label{fig:product_of_elements}
\end{figure}

\subsection{Polynomial calculations}

Empirical showed time complexity of $\Theta(n)$, which matches theoretical time complexity.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{data/polynomial_direct_calculation.pdf}
	\caption{Performance of direct calculation (polynomial)}
	\label{fig:polynomial_direct_calculation}
\end{figure}

\subsection{Polynomial Horner's method}

Empirical showed time complexity of $\Theta(n)$, which matches theoretical time complexity.\\

To successfully evaluate the algorithm in the whole range of input sizes was necessary to configure the stack size to avoid OverflowError errors.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{data/polynomial_horner.pdf}
	\caption{Performance of Horner's method (polynomial)}
	\label{fig:polynomial_horner}
\end{figure}

\subsection{Bubble Sort}

Empirical showed time complexity of $\Theta(n^2)$, which matches theoretical time complexity.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{data/bubble_sort.pdf}
	\caption{Time performance of bubble sort}
	\label{fig:bubble_sort}
\end{figure}

\subsection{Quick Sort}

Different implementations were tested. The initial QuickSort algorithm was implemented in a way that took advantage of the dynamic nature of Python lists. Finally, we tried an algorithm that did in-place substitution of the elements of the array. All implementations showed an empirical time complexity of $\Theta(n^2)$, which matches the theoretical worst-case time complexity.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{data/quick_sort.pdf}
	\caption{Time performance of quick sort}
	\label{fig:quick_sort}
\end{figure}

\subsection{Tim Sort}

Empirical showed time complexity of $\Theta(n)$, which matches best-case theoretical time complexity.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{data/tim_sort.pdf}
	\caption{Time performance of tim sort}
	\label{fig:tim_sort}
\end{figure}

\subsection{Matrix product}

In this case, the function under study wasn't executed for the whole range of input values, because after 6 hours the test didn't finish.

Here we present the test, executed with arrays of lengths from 1 to 413, which was enough to prove that empirical time complexity is $\Theta(n^3)$, which matches theoretical time complexity.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{data/matrix_product.pdf}
	\caption{Time performance of matrix product}
	\label{fig:matrix_product}
\end{figure}

%--------------------------------------------------
% section
%--------------------------------------------------

\newpage

\section{Conclusion}

The empirical time complexity of functions under study matched the theoretical time complexity.\\

For some cases, the approximation fitted the data so well that a change in the
representation was required, to be able to visually differentiate data and
approximation (see Figure~\ref{fig:bubble_sort} on page \pageref{fig:bubble_sort}).\\

A different case is QuickSort (see Figure~\ref{fig:quick_sort} on page \pageref{fig:quick_sort}). The approximations are very close to the actual data, but some variability was introduced  in the second half of the test. Further analysis is required to find the source of that variability, or a different method could be used to analyze the data: for example, use median instead of average, or use the minimal value of each run instead of the average.

%--------------------------------------------------
% Bibliography
%--------------------------------------------------

\newpage

\section{Bibliography and references}

\begin{itemize}

	\item Anany Levitin. (2011). \textit{Introduction to the Design and Analysis of Algorithms}. Pearson.

	\item Nicolas Auger, Cyril Nicaud, Carine Pivoteau. Merge Strategies: from Merge Sort to TimSort. 2015. ffhal-01212839v2 https://hal-upec-upem.archives-ouvertes.fr/hal-01212839v2/document

	\item Richard Scheiwe \textit{The Case for Timsort}.\\ https://medium.com/@rscheiwe/the-case-for-timsort-349d5ce1e414

	\item Tim Peters \textit{Official documentation on Tim Sort (Python Implementation)}.\\ https://github.com/python/cpython/blob/master/Objects/listsort.txt

	\item \textit{Big-O complexities of common algorithms used in computer science}\\ http://blogs.longwin.com.tw/wordpress/201608-bigoposter.pdf

	\item Intel® Core™ i7-8850H Processor data sheet\\ https://ark.intel.com/content/www/us/en/ark/products/134899/intel-core-i7-8850h-processor-9m-cache-up-to-4-30-ghz.html

\end{itemize}

%--------------------------------------------------
% Appendix
%--------------------------------------------------

\newpage

\section{Appendix}

Source code can be found at \textit{https://gitlab.com/hgdeoro/itmo-aadoa-task-1}, including:

\begin{itemize}
	\item Python code used to get empirical data and plots
	\item Python code of algorithms
	\item Data in form of CSV values (in the \textit{data} directory)
\end{itemize}

\newpage

\subsection{Code for sorting algorithms}

Also available in GitLab repository at \textit{/itmo/sorting.py}.

\lstinputlisting[language=Python, caption=Sorting]{itmo/sorting.py}

\newpage

\subsection{Code for polynomial operations}

Also available in GitLab repository at \textit{/itmo/polynomial.py}.

\lstinputlisting[language=Python, caption=Polynomial operations]{itmo/polynomial.py}

\newpage

\subsection{Code for matrix operations}

Also available in the GitLab repository at \textit{/itmo/matrix.py}.

\lstinputlisting[language=Python, caption=Matrix operations]{itmo/matrix.py}

\newpage

\subsection{Support code: base class for measure time performance}

Also available in GitLab repository at \textit{/itmo/measure.py}.

\lstinputlisting[language=Python, caption=Support code: base class for measure time performance]{itmo/measure.py}

\newpage

\subsection{Support code: running of algorithms and plotting}

Also available in the GitLab repository at \textit{/task\_1.py}.

\lstinputlisting[language=Python, caption=Support code: running of algorithms and plotting]{task_1.py}

\end{document}
