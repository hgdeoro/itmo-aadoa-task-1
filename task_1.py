import logging
import os
import pathlib
import sys

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.axes import Axes
from matplotlib.figure import Figure

from itmo import sorting, measure, polynomial, matrix


class RandomArrayMeasure(measure.Measure):
    F_ARG_SIZE = 'size'

    def __init__(self,
                 array_size_min: int,
                 array_size_max: int,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._array_size_min: int = array_size_min
        self._array_size_max: int = array_size_max

    def _get_f_args(self):
        for size in range(self._array_size_min, self._array_size_max + 1):
            yield {
                self.F_ARG_SIZE: size,
            }


class RandomMatrixMeasure(measure.Measure):
    F_ARG_SIZE = 'size'

    def __init__(self,
                 matrix_size_min: int,
                 matrix_size_max: int,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._matrix_size_min: int = matrix_size_min
        self._matrix_size_max: int = matrix_size_max

    def _get_f_args(self):
        for size in range(self._matrix_size_min, self._matrix_size_max + 1):
            yield {
                self.F_ARG_SIZE: size,
            }


class ConstantFunction(RandomArrayMeasure):

    def build_function_data(self, size):
        return self._create_random_list(size=(size,))

    def run_function(self, array):
        return array[0]


def sum_of_array(array):
    total = 0
    for item in array:
        total = total + item
    return total


class SumOfElements(RandomArrayMeasure):

    def build_function_data(self, size):
        return self._create_random_list(size=(size,))

    def run_function(self, array):
        return sum_of_array(array)


def product(array):
    total = 1
    for item in array:
        total = total * item
    return total


class ProductOfElements(RandomArrayMeasure):

    def build_function_data(self, size):
        return self._create_random_list(size=(size,))

    def run_function(self, array):
        return product(array)


class PolinomialDirectCalculation(RandomArrayMeasure):

    def build_function_data(self, size):
        return self._create_random_list(size=(size,))

    def run_function(self, array):
        return polynomial.direct_calculation(1.5, array)


class PolinomialHorner(RandomArrayMeasure):

    def build_function_data(self, size):
        return self._create_random_list(size=(size,))

    def run_function(self, array):
        return polynomial.horners_method(1.5, array)


class BubbleSort(RandomArrayMeasure):

    def build_function_data(self, size):
        return self._create_random_list(size=(size,))

    def run_function(self, array):
        return sorting.bubble_sort(array)


class QuickSort(RandomArrayMeasure):
    def build_function_data(self, size):
        return self._create_random_list(size=(size,))

    def run_function(self, array):
        return sorting.quicksort(array)


class TimSort(RandomArrayMeasure):
    def build_function_data(self, size):
        return self._create_random_list(size=(size,))

    def run_function(self, array):
        return array.sort()


class MatrixProduct(RandomMatrixMeasure):
    def build_function_data(self, size):
        matrix_a = self._create_random_list(size=(size, size))
        matrix_b = self._create_random_list(size=(size, size))
        return matrix_a, matrix_b

    def run_function(self, arrays):
        matrix_a, matrix_b = arrays
        return matrix.matrix_product(matrix_a, matrix_b)


class Plot:
    def plot_polyfit_polynomial(self,
                                csv_path: pathlib.Path,
                                polyfit_deg: int,
                                x_label: str,
                                y_label: str,
                                data_line_style='-',
                                approx_line_style='-'):
        assert csv_path.exists()
        assert csv_path.is_file()
        assert csv_path.name.endswith('.csv')
        output_plot_path = csv_path.parent.joinpath(f"{csv_path.name[0:-4]}.pdf")

        print(f"Plotting {csv_path.name} -> {output_plot_path.name}")
        data = np.genfromtxt(csv_path,
                             delimiter=",",
                             dtype=float,
                             skip_header=1)
        x = data[:, 0].astype(int)
        y = data[:, 1]

        polyfit = np.polyfit(x, y, deg=polyfit_deg)
        polynomial = np.poly1d(polyfit)

        fig, ax = plt.subplots()
        fig: Figure
        ax: Axes

        ax.get_yaxis().set_major_formatter(
            matplotlib.ticker.FuncFormatter(lambda value, p: format(int(value), ',')))

        ax.plot(x, y, data_line_style, alpha=0.5, label=x_label)
        ax.plot(x, polynomial(x), approx_line_style, label=y_label)
        ax.set_xlabel('Array size')
        ax.set_ylabel('Time (ms)')
        ax.legend()
        fig.savefig(str(output_plot_path))


def main():
    actions = sys.argv[1:]
    assert actions, f"Action to perform not provided"

    TEST_MODE = os.environ.get('TEST_MODE', 'false').lower() == 'true'

    constant_function_csv = pathlib.Path('./data/constant_function.csv')
    sum_of_elements_csv = pathlib.Path('./data/sum_of_elements.csv')
    product_of_elements_csv = pathlib.Path('./data/product_of_elements.csv')
    polynomial_direct_calculation_csv = pathlib.Path('./data/polynomial_direct_calculation.csv')
    polynomial_horner_csv = pathlib.Path('./data/polynomial_horner.csv')
    bubble_sort_csv = pathlib.Path('./data/bubble_sort.csv')
    quick_sort_csv = pathlib.Path('./data/quick_sort.csv')
    tim_sort_csv = pathlib.Path('./data/tim_sort.csv')
    matrix_product_csv = pathlib.Path('./data/matrix_product.csv')

    if 'CSV' in actions:
        # constant_function
        if TEST_MODE or not constant_function_csv.exists():
            ConstantFunction(array_size_min=1 if not TEST_MODE else 1980,
                             array_size_max=2000,
                             repeat=5,
                             output_filename=constant_function_csv,
                             output_f_args=['size']).run()

        # sum_of_elements
        if TEST_MODE or not sum_of_elements_csv.exists():
            SumOfElements(array_size_min=1 if not TEST_MODE else 1980,
                          array_size_max=2000,
                          repeat=5,
                          output_filename=sum_of_elements_csv,
                          output_f_args=['size']).run()

        # product_of_elements
        if TEST_MODE or not product_of_elements_csv.exists():
            ProductOfElements(array_size_min=1 if not TEST_MODE else 1980,
                              array_size_max=2000,
                              repeat=5,
                              output_filename=product_of_elements_csv,
                              output_f_args=['size']).run()

        # polynomial_direct_calculation
        if TEST_MODE or not polynomial_direct_calculation_csv.exists():
            PolinomialDirectCalculation(array_size_min=1 if not TEST_MODE else 1980,
                                        array_size_max=2000,
                                        repeat=5,
                                        output_filename=polynomial_direct_calculation_csv,
                                        output_f_args=['size']).run()

        # polynomial_horner
        if TEST_MODE or not polynomial_horner_csv.exists():
            PolinomialHorner(array_size_min=1 if not TEST_MODE else 1980,
                             array_size_max=2000,
                             repeat=5,
                             output_filename=polynomial_horner_csv,
                             output_f_args=['size']).run()

        # bubble_sort
        if TEST_MODE or not bubble_sort_csv.exists():
            BubbleSort(array_size_min=1,
                       array_size_max=2000 if not TEST_MODE else 300,
                       repeat=5,
                       output_filename=bubble_sort_csv,
                       output_f_args=['size']).run()

        # quick_sort
        if TEST_MODE or not quick_sort_csv.exists():
            QuickSort(array_size_min=1 if not TEST_MODE else 1980,
                      array_size_max=2000,
                      repeat=5,
                      output_filename=quick_sort_csv,
                      output_f_args=['size']).run()

        # tim_sort
        if TEST_MODE or not tim_sort_csv.exists():
            TimSort(array_size_min=1 if not TEST_MODE else 1980,
                    array_size_max=2000,
                    repeat=5,
                    output_filename=tim_sort_csv,
                    output_f_args=['size']).run()

        if TEST_MODE or not matrix_product_csv.exists():
            MatrixProduct(matrix_size_min=1,
                          matrix_size_max=2000 if not TEST_MODE else 50,
                          repeat=5,
                          output_filename=matrix_product_csv,
                          output_f_args=['size']).run()

    if 'PLOT' in actions:
        # constant_function
        Plot().plot_polyfit_polynomial(
            csv_path=constant_function_csv,
            polyfit_deg=0,
            x_label="Constant Function execution time",
            y_label="$\\Theta(1)$ approximation",
        )

        # sum_of_elements
        Plot().plot_polyfit_polynomial(
            csv_path=sum_of_elements_csv,
            polyfit_deg=1,
            x_label="Sum Of Elements execution time",
            y_label="$\\Theta(n)$ approximation",
        )

        # product_of_elements
        Plot().plot_polyfit_polynomial(
            csv_path=product_of_elements_csv,
            polyfit_deg=2,
            x_label="Product Of Elements execution time",
            y_label="$\\Theta(n^2)$ approximation",
        )

        # polynomial_direct_calculation
        Plot().plot_polyfit_polynomial(
            csv_path=polynomial_direct_calculation_csv,
            polyfit_deg=1,
            x_label="Direct calculation (polynomial) execution time",
            y_label="$\\Theta(n)$ approximation",
        )

        # polynomial_horner
        Plot().plot_polyfit_polynomial(
            csv_path=polynomial_horner_csv,
            polyfit_deg=1,
            x_label="Horner's method execution time",
            y_label="$\\Theta(n)$ approximation",
        )

        # bubble_sort
        Plot().plot_polyfit_polynomial(
            csv_path=bubble_sort_csv,
            polyfit_deg=2,
            x_label="Bubble Sort execution time",
            y_label="$\\Theta(n^2)$ approximation",
            data_line_style='+',
        )

        # quick_sort
        Plot().plot_polyfit_polynomial(
            csv_path=quick_sort_csv,
            polyfit_deg=2,
            x_label="Quick Sort execution time",
            y_label="$\\Theta(n^2)$ approximation",
        )

        # tim_sort
        Plot().plot_polyfit_polynomial(
            csv_path=tim_sort_csv,
            polyfit_deg=1,
            x_label="Tim Sort execution time",
            y_label="$\\Theta(n)$ approximation",
        )

        # matrix_product
        Plot().plot_polyfit_polynomial(
            csv_path=matrix_product_csv,
            polyfit_deg=3,
            x_label="Matrix product execution time",
            y_label="$\\Theta(n^3)$ approximation",
            data_line_style='+'
        )


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    sys.setrecursionlimit(2100)
    main()
