try:
    elapsed_sec = 0.0
    # Temporarily disable garbage colector:
    gc.disable()
    try:
        for _ in range(self._repeat):
            # Create data required to execute altorithm:
            function_data = self.build_function_data(**f_args)
            # Use monotonic clock:
            timestamp_start = time.monotonic()
            self.run_function(function_data)
            # Use monotonic clock:
            timestamp_end = time.monotonic()
            elapsed_sec += (timestamp_end - timestamp_start)
    except OverflowError:
        elapsed_sec = None
finally:
    gc.enable()
